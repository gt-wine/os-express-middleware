# Winecraft Express Middleware


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Versioning](#versioning)
- [Support](#support)
- [Contributing](#contributing)

## Installation

### General Setup
```
$ npm install
```

## Usage

This package is a collection of middlwares for Express serverless apps on AWS.
```
const express = require('express');
const AppMiddleware = require('winecraft-os-express-middleware');
/**
 * Instantiate the App
 */
const app = express();
/**
 * Adds CORS headers to all responses
 */
app.use(AppMiddleware.corsHeaders);

```

## Versioning

This project uses semantic versioning - https://semver.org/

To create a new version run the following command

```
$ npm version {major|minor|patch}
```

## Support

Adam Synnott <adam.synnott@winecraft.com>

## Contributing

Please contribute using [Github Flow](https://guides.github.com/introduction/flow/).
