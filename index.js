const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const AWS = require('aws-sdk');
const HttpStatus = require('http-status-codes');

const PROJECT_ROOT = `${__dirname}/../..`;
const SSM_PATH_SEPARATOR = process.env.SSM_PATH_SEPARATOR ? process.env.SSM_PATH_SEPARATOR : '/';
const SSM_PATH = process.env.SSM_PATH ? process.env.SSM_PATH : `/${process.env.ENVIRONMENT}/Winecraft/API/`;
const INDEX_HTML_PATH = process.env.INDEX_HTML_PATH ? process.env.INDEX_HTML_PATH : '../../dist/index.html';
const DEFAULT_OPEN_GRAPH_URL = process.env.DEFAULT_OPEN_GRAPH_URL ? process.env.DEFAULT_OPEN_GRAPH_URL : 'https://winecraft.com/static/open-graph-1200x630.jpg';

let map = null;
if (fs.existsSync(`${PROJECT_ROOT}/meta/map.json`)) {
  map = require(`${PROJECT_ROOT}/meta/map.json`);
}

let packageJSON = null;
if (fs.existsSync(`${PROJECT_ROOT}/package.json`)) {
  packageJSON = require(`${PROJECT_ROOT}/package.json`);
}

let redirects;
if (fs.existsSync(`${PROJECT_ROOT}/meta/redirects.json`)) {
  redirects = require(`${PROJECT_ROOT}/meta/redirects.json`);
}

class AppMiddleware {
  /**
   * Middleware to return an OK status for the /health
   * route regardless of protocol - http or https
   * @param Object req
   * @param Object res
   * @param Object next
   */
  static healthCheck (req, res, next) {
    if (req.url === '/health') {
      // @TODO other health checks could be added here
      // - check if api is up and working
      // - check if prerender is working etc
      const version = packageJSON ? packageJSON.version : 'undefined';
      res.json({
        status: 'OK',
        version,
      })
    } else {
      return next();
    }
  }
  /**
   * Adds CORS Headers to each response
   * @param Object    req
   * @param Object    res
   * @param function  next
   */
  static corsHeaders(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    return next();
  }
  /**
   * Adds a useful library of HTTP Status codes for use in controllers and middleware when sending a
   * response. e.g. res.statusCodes.NOT_FOUND.
   * @param Object    req
   * @param Object    res
   * @param function  next
   */
  static httpStatusCodes(req, res, next) {
    res.httpStatusCodes = HttpStatus;
    return next();
  }
  /**
   * grab env vars from ssm on instantiation
   * @param Object    req
   * @param Object    res
   * @param function  next
   */
  static async populateEnvironmentVariables(req, res, next) {
    // skip this on development environments`
    if (_.isEmpty(process.env.ENVIRONMENT) || process.env.ENVIRONMENT === 'development') {
      return next();
    }
    let response;
    const ssm = new AWS.SSM({ apiVersion: '2014-11-06' });
    const params = {
      Path: SSM_PATH,
      WithDecryption: true,
    };
    try {
      while (response = await ssm.getParametersByPath(params).promise()) {
        if(!_.isEmpty(response.NextToken)) {
          params.NextToken = response.NextToken;
        }
        _.each(response.Parameters, (value) => {
          process.env[_.last(_.split(value.Name, SSM_PATH_SEPARATOR))] = value.Value;
        });
        if(_.isEmpty(response.NextToken)) {
          return next();
        }
      }
    } catch (error) {
      console.log('[SSM Error]', error);
      res.status(res.httpStatusCodes.INTERNAL_SERVER_ERROR).json({ error: error.message });
    }
  }
    /**
   * Middleware to redirect to https if required from
   * behind a loadbalancer
   * @param Object req
   * @param Object res
   * @param Object next
   */
  static secureRedirect (req, res, next) {
    if (req.headers['x-forwarded-proto'] === 'https') {
      return next();
    } else {
      res.redirect(301, `https://${req.headers.host}${req.url}`);
    }
  }
  /**
   * Middleware to attach the index html to the request locals
   * @param Object req
   * @param Object res
   * @param Object next
   */
  static attachIndexHtmlToRequest (req, res, next) {
    const indexHTML = (() => fs.readFileSync(path.resolve(__dirname, INDEX_HTML_PATH), 'utf-8'))();
    res.locals.indexHTML = indexHTML;
    return next();
  }
  /**
   * Middleware to replace any defined meta tags based on
   * the meta map
   * @param Object req
   * @param Object res
   * @param Object next
   */
  static addMetaTagsToHtml (req, res, next) {
    if (!map) {
      return next();
    }
    const { indexHTML } = res.locals;
    const { pathname } = req._parsedUrl;
    // remove the last slash from the url if it exists
    // replace all forward slashes with dashes
    const key = pathname.substring(1).replace(/\//g, '-').replace(/-\s*$/, '');
    if (typeof map[key] !== 'undefined') {
      if (map[key].image) {
        res.locals.indexHTML = indexHTML.replace(DEFAULT_OPEN_GRAPH_URL, map[key].image)
      }
    }
    return next();
  }
  /**
   *
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  static handleRedirect(req, res, next) {
    if (!redirects) {
      return next();
    }
    const pathname = (req._parsedUrl.pathname).replace(/\/$/, "");
    const query = req._parsedUrl.query ? `?${req._parsedUrl.query}` : '';
    if (typeof redirects[pathname] !== 'undefined') {
      console.log('redirecting', req.url, `${redirects[pathname]}?${req._parsedUrl.query}`);
      res.redirect(301, `${redirects[pathname]}${query}`);
    } else {
      return next();
    }
  }
}

module.exports = AppMiddleware;
